package ictgradschool.industry.lab_exceptions.ex04;

public class IndexTooHighException extends Exception {
    public IndexTooHighException(String message) {
        super(message);
        System.out.println(message);
    }
}